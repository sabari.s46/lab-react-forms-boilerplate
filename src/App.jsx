import React, { Component } from "react";
import "./App.css";

const initialState = {
  first_name: "",
  last_name: "",
  email: "",
  contact: "",
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formValues: { ...initialState },
      formError: {},
      success: false,
      isSubmit: false,
    };
  }

  handleChange = (e) => {
    const { id, value } = e.target;
    this.setState((prevState) => ({
      formValues: { ...prevState.formValues, [id]: value },
    }));
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const formError = this.checkError(this.state.formValues);
    this.setState({
      formError,
      isSubmit: true,
      success: Object.keys(formError).length === 0,
    });
  };

  checkError = (values) => {
    const errors = {};
    const regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;

    if (values.first_name.trim() === "") {
      errors.first_name = "Please enter your first name";
    }

    if (values.last_name.trim() === "") {
      errors.last_name = "Please enter your last name";
    }

    if (values.email.trim() === "") {
      errors.email = "Please enter your email";
    } else if (!regex.test(values.email)) {
      errors.email = "Please enter a valid email";
    }

    if (values.contact.trim() === "") {
      errors.contact = "Please enter your contact";
    } else if (values.contact.length !== 10) {
      errors.contact = "Please enter a valid contact";
    }

    return errors;
  };

  render() {
    const { formValues, formError, success } = this.state;

    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div id="items">
            {success ? (
              <p className="success">Registration success</p>
            ) : (
              <p></p>
            )}
            <input
              type="text"
              id="first_name"
              value={formValues.first_name}
              onChange={this.handleChange}
              placeholder="First Name"
            />
            <p className="error">{formError.first_name}</p>
            <input
              type="text"
              id="last_name"
              value={formValues.last_name}
              onChange={this.handleChange}
              placeholder="Last Name"
            />
            <p className="error">{formError.last_name}</p>
            <input
              type="text"
              id="email"
              value={formValues.email}
              onChange={this.handleChange}
              placeholder="Email"
            />
            <p className="error">{formError.email}</p>
            <input
              type="number"
              id="contact"
              value={formValues.contact}
              onChange={this.handleChange}
              placeholder="Contact"
            />
            <p className="error">{formError.contact}</p>
            <button type="submit">Register</button>
          </div>
        </form>
      </>
    );
  }
}

export default App;
